This project was bootstrapped with [Create React App]

-----------------------------------------------------

Key features:

- Stateless functional component
- ES6 class component
- Stunning interface (I know, Looks so similar but I'd prefer my version)
- Using of code standards
- Code versioned

 TODO:

 - Create an effective state management
 - UI tests
 - Unit tests
 - Deploy on heroku or similar service
 - Configure webpack properly


 How to start: 

- To begin the development, run `npm start` or `yarn start`.
- To create a production bundle, use `npm run build` or `yarn build`.

Mounting lifecycle - invoked when component are mounted and unmouted.

Updating lifecycle - invoked when state changes

lets use libs:

isomorphic-fetch - to work with fetch API

*Use data is beatiful to show to create a new web-app in react and try to pulish.

- High order components