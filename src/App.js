import React, { Component } from 'react';
import logo from './mathspace.svg';
import './App.css';
import Recommendations from './components/Recommendations';
import MemberList from './components/MemberList';
//import Timeline from './components/Timeline';
import CountryList from './components/CountryList';


const data = require('./components/data.json');
const historicDatesForSkiing = require('./components/historic.json');

class App extends Component {
  render() {
    return (
      <div className="App">
        
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>

        <Recommendations subjects={data} />

        <MemberList />

        {/* TODO: Fix bug on D3 */}
        {/* <Timeline name="History of Skiing"
              data={historicDatesForSkiing} /> */}

        <CountryList />

      </div>
    );
  }
}

export default App;
