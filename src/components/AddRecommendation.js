import React from 'react';
import './AddRecommendation.css';

// optional function property onNewRecommendation
// in order to make two-way data binding optional
const AddRecommendation = ({onNewRecommendation=f=>f}) => {
    
    let _badge, _name

    const submit = e => {
        e.preventDefault()
        onNewRecommendation(_badge.value, _name.value)
        _badge.value = ''
        _name.value = ''
        _badge.focus()
    }

    return (
        <form onSubmit={submit} className="recommendation-form">
        
            <label>Recommend a new course</label>
            
            <input ref={input => _badge = input}
                   type="text" 
                   placeholder="badge url..." required/>
            
            <input ref={input => _name = input}
                   type="text" 
                   placeholder="recommendation name..." required/>
            
            <button>Suggest</button>
        </form>
    )
}

export default AddRecommendation;