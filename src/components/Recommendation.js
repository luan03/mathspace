import React from 'react';
import PropTypes from "prop-types";

const Recommendation = ({ badge, name }) => 

<li>
    <img src={badge} alt={name} title={name} width="62" height="62" /> 
    {name}
</li>

Recommendation.propTypes = {
    badge: PropTypes.string,
    name: PropTypes.string.isRequired
};

Recommendation.defaultProps = {
    badge: "image-default.png",
    name: "Subject not available"
  };

export default Recommendation;