import React from 'react';
import './Recommendations.css';

import Recommendation from './Recommendation';
import AddRecommendation from './AddRecommendation';

const Recommendations = ({ subjects }) => 
    <section className="recommendations">

        <h1>Recommendations</h1>
        
        <ul className="list">
            {
                subjects.map((recommendation, i) =>
                <Recommendation key={i} {...recommendation} />)
            }
        </ul>

        <span className="button" title="No thanks, I'll choose my own">No thanks, I'll choose my own</span>
        

        <AddRecommendation onNewRecommendation={(badge, name) => {
            console.log(`Recommendation: ${badge} ${name} was added to the list`);
        }} />

    </section>

export default Recommendations;